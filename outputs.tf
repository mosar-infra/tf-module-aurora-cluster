# aurora_cluster/outputs.tf

output "db_endpoint" {
  value = aws_rds_cluster.cluster.endpoint
}
output "db_hostname" {
  value = split(":", aws_rds_cluster.cluster.endpoint)[0]
}

output "db_arn" {
  value = aws_rds_cluster.cluster.arn
}

output "db" {
  value = aws_rds_cluster.cluster
  sensitive = true
}

output "db_name" {
  value = aws_rds_cluster.cluster.database_name
}
