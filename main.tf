# aurora_cluster/main.tf

resource "aws_rds_cluster" "cluster" {
  availability_zones                  = var.availability_zones
  backtrack_window                    = var.backtrack_window
  backup_retention_period             = var.backup_retention_period
  cluster_identifier                  = var.cluster_identifier
  copy_tags_to_snapshot               = var.copy_tags_to_snapshot
  database_name                       = var.database_name
  db_subnet_group_name                = var.db_subnet_group_name
  deletion_protection                 = var.deletion_protection
  enable_http_endpoint                = var.enable_http_endpoint
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports
  engine                              = var.engine
  engine_mode                         = "serverless"
  engine_version                      = var.engine_version
  final_snapshot_identifier           = var.final_snapshot_identifier
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  master_password                     = var.master_password
  master_username                     = var.master_username
  preferred_maintenance_window        = var.preferred_maintenance_window

  scaling_configuration {
      auto_pause               = var.scaling_configuration_auto_pause
      max_capacity             = var.scaling_configuration_max_capacity
      min_capacity             = var.scaling_configuration_min_capacity
      seconds_until_auto_pause = var.scaling_configuration_seconds_until_auto_pause
      timeout_action           = var.scaling_configuration_timeout_action
  }

  skip_final_snapshot = var.skip_final_snapshot
  storage_encrypted   = var.storage_encrypted

  tags = merge(var.tags, {
    Name = var.cluster_identifier
  })

  vpc_security_group_ids = var.vpc_security_group_ids
}
